package com.teamsankya.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.teamsankya.dao.EmployeeDAO;
import com.teamsankya.dto.EmployeeBean;
import com.teamsankya.util.EmployeeDAOFactory;

@WebServlet("/search")
public class SearchEmployeeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int eid = Integer.parseInt(req.getParameter("eid"));
		EmployeeDAO dao = EmployeeDAOFactory.getEmployeeDAOInstance();
		
		EmployeeBean bean = dao.getEmployee(eid);
		req.setAttribute("bean", bean);
		req.getRequestDispatcher("/SearchResponce.jsp")
						.forward(req, resp);
	}
}
