package com.teamsankya.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.teamsankya.dao.EmployeeDAO;
import com.teamsankya.dto.EmployeeBean;
import com.teamsankya.util.EmployeeDAOFactory;

@WebServlet("/createEmployee")
public class CreateEmployeeServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		EmployeeBean bean = new EmployeeBean();
		bean.setFname(req.getParameter("fname"));
		bean.setLname(req.getParameter("lname"));
		bean.setAddress1(req.getParameter("address1"));
		bean.setAddress2(req.getParameter("address2"));
		bean.setCity(req.getParameter("city"));
		bean.setEmail(req.getParameter("email"));
		bean.setPhno(Integer.parseInt(req.getParameter("phno")));
		bean.setPincode(Integer.parseInt(req.getParameter("pincode")));
		bean.setEid(Integer.parseInt(req.getParameter("eid")));
		
		EmployeeDAO dao = EmployeeDAOFactory.getEmployeeDAOInstance();
		dao.createEmployee(bean);//DTO
		
		req.getRequestDispatcher("CreateEmployeeResponse.jsp")
						.forward(req, resp);
	}// end of doPost

}// end CreateEmployeeServlet











