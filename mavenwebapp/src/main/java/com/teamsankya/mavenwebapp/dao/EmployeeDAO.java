package com.teamsankya.dao;

import com.teamsankya.dto.EmployeeBean;

public interface EmployeeDAO {
	public void createEmployee(EmployeeBean bean);
	public EmployeeBean getEmployee(int eid);
}
