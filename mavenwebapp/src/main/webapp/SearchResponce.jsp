<%@page import="com.teamsankya.dto.EmployeeBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<%
	EmployeeBean bean = (EmployeeBean)request.getAttribute("bean");
%>
<body>
<%if(bean!=null) {%>
	<table>
		<tr>
			<td>Eid</td>
			<td>Fname</td>
			<td>Lname</td>
			<td>Email</td>
			<td>Ph No.</td>
			<td>Address1</td>
			<td>Address2</td>
			<td>City</td>
			<td>Pincode</td>
		</tr>
		<tr>
			<td><%=bean.getEid() %></td>
			<td><%=bean.getFname() %></td>
			<td><%=bean.getLname() %></td>
			<td><%=bean.getEmail() %></td>
			<td><%=bean.getPhno() %></td>
			<td><%=bean.getAddress1() %></td>
			<td><%=bean.getAddress2() %></td>
			<td><%=bean.getCity() %></td>
			<td><%=bean.getPincode() %></td>
		</tr>
	</table>
	<%}else { %>
	<h1>Employee id does not exist</h1>
	<%} %>
	<a href='./index.jsp'>Click here to insert more employee</a><br>
	<a href='./Search.jsp'>Click here to search an employee</a>
</body>
</html>